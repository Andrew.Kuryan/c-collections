//
// Created by andrew on 30.11.18.
//

#include <stdlib.h>
#include "include/mapelement.h"

MapElement map_element_new(Basic key, Basic value){
    MapElement element = (MapElement) calloc(1, sizeof(struct _MapElement));

    element->key = key;
    element->value = value;
    element->next = NULL;

    return element;
}

void map_element_destroy(MapElement element){
    free(element->key->value);
    free(element->value->value);
    free(element->key);
    free(element->value);
    free(element);
}