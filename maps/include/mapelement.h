//
// Created by andrew on 30.11.18.
//

#ifndef COLLECTIONS_MAPELEMENT_H
#define COLLECTIONS_MAPELEMENT_H

#include "../../basicclasses/include/basic.h"

#define MapElement struct _MapElement*

struct _MapElement{
    Basic key;
    Basic value;
    MapElement next;
};

MapElement map_element_new(Basic key, Basic value);
void map_element_destroy(MapElement element);

#endif //COLLECTIONS_MAPELEMENT_H
