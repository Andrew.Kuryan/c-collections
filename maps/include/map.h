//
// Created by andrew on 30.11.18.
//

#ifndef COLLECTIONS_MAP_H
#define COLLECTIONS_MAP_H

#include "mapelement.h"
#include "../../basicclasses/include/string.h"
#include "../../lists/lists.h"

#define Map struct _Map*

struct _Map{
    int kType;
    int vType;
    int size;
    MapElement* head;
};

Map map_new(int kType, int vType);
String map_to_string(Map map);

int map_put(Map map, Basic key, Basic value);
Basic map_get(Map map, Basic key);
int map_set(Map map, Basic key, Basic newVal);
Basic map_delete(Map map, Basic key);
ArrayList map_key_set(Map map);
int map_size(Map map);

#endif //COLLECTIONS_MAP_H
