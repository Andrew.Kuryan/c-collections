//
// Created by andrew on 30.11.18.
//

#include <stdio.h>
#include "include/map.h"
#include "../collections.h"

Map map_new(int kType, int vType){
    Map map = (Map) calloc(1, sizeof(struct _Map));

    map->kType = kType;
    map->vType = vType;
    map->size = 0;
    map->head = NULL;

    return map;
}

String map_to_string(Map map){
    String res = str_new("");
    res = str_concat(res, str_new("["));
    MapElement temp;
    for (int i=0; i<map->size-1; i++){
        temp = map->head[i];
        res = str_concat(res, str_value_of(temp->key));
        res = str_concat(res, str_new("="));
        res = str_concat(res, str_value_of(map_get(map, temp->key)));
        res = str_concat(res, str_new(", "));
    }
    res = str_concat(res, str_value_of(map->head[map->size-1]->key));
    res = str_concat(res, str_new("="));
    res = str_concat(res, str_value_of(map_get(map, map->head[map->size-1]->key)));
    res = str_concat(res, str_new("]"));
    return res;
}

int map_size(Map map){
    return map->size;
}

int map_put(Map map, Basic key, Basic value){
    if (value->type != map->vType || key->type != map->kType)
        return 0;
    if (map->head == NULL){
        map->head = (MapElement*) calloc(1, sizeof(MapElement));
        map->head[0] = map_element_new(key, value);
        map->size++;
    }
    else{
        if (map_get(map, key) != NULL)
            return 0;
        map->head = (MapElement*) realloc(map->head, (map->size+1) * sizeof(MapElement));
        map->size++;
        map->head[map->size-1] = map_element_new(key, value);
    }
    return 1;
}

Basic map_get(Map map, Basic key){
    if (key->type != map->kType)
        return NULL;
    for (int i=0; i<map->size; i++){
        MapElement temp = map->head[i];
        switch (key->type){
            case INT_T:
                if (INT_UNPACK(key) == INT_UNPACK(temp->key))
                    return temp->value;
                break;
            case DOUBLE_T:
                if (DOUBLE_UNPACK(key) == DOUBLE_UNPACK(temp->key))
                    return temp->value;
                break;
            case STRING_T:
                if (str_compare_to(STR_CLASS(key), STR_CLASS(temp->key)) == 0)
                    return temp->value;
                break;
            default:
                return NULL;
        }
    }
    return NULL;
}

int map_set(Map map, Basic key, Basic newVal) {
    if (newVal->type != map->vType || key->type != map->kType)
        return 0;
    *map_get(map, key) = *newVal;
    return 1;
}

Basic map_delete(Map map, Basic key){
    if (key->type != map->kType)
        return NULL;
    MapElement del;
    for (int i=0; i<map->size; i++){
        MapElement temp = map->head[i];
        del = NULL;
        switch (key->type){
            case INT_T:
                if (INT_UNPACK(key) == INT_UNPACK(temp->key))
                    del =  temp;
                break;
            case DOUBLE_T:
                if (DOUBLE_UNPACK(key) == DOUBLE_UNPACK(temp->key))
                    del =  temp;
                break;
            case STRING_T:
                if (STR_UNPACK(key) == STR_UNPACK(temp->key))
                    del =  temp;
                break;
            default:
                return NULL;
        }
        if (del != NULL){
            for (int j=i; j<map->size-1; j++){
                map->head[j] = map->head[j+1];
            }
            map_element_destroy(del);
            map->size--;
        }
    }
}

ArrayList map_key_set(Map map){
    ArrayList set = array_list_new(map->kType);
    for (int i=0; i<map->size; i++){
        MapElement temp = map->head[i];
        array_list_add(set, temp->key);
    }
    return set;
}