//
// Created by andrew on 05.10.18.
//

#ifndef COLLECTIONS_DOUBLE_H
#define COLLECTIONS_DOUBLE_H

#include "basic.h"
#include "string.h"

#define Double struct _Double*
#define DOUBLE(a) double_class_to_double(a)
#define DOUBLE_CLASS(a) basic_to_double_class(a)
#define DOUBLE_PACK(a) double_to_basic(a)
#define DOUBLE_UNPACK(a) basic_to_double(a)

struct _Double{
    int type;
    double* value;
};

Double double_new(double a);
double double_class_to_double(Double _double);
Double basic_to_double_class(Basic basic);
Basic double_to_basic(double a);
double basic_to_double(Basic basic);

Double parse_double(String str);

#endif //COLLECTIONS_DOUBLE_H
