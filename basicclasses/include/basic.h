//
// Created by andrew on 05.10.18.
//

#ifndef COLLECTIONS_BASIC_H
#define COLLECTIONS_BASIC_H

#include <stdlib.h>
#include "types.h"

#define MARE_STR(name) #name
#define BASIC(num) basic_new(num)
#define VAL(num) get_value(num)

#define USE_BASIC_SHELL(name,\
                        NAME)\
    name NAME##_UNPACK(Basic basic);\
    Basic NAME##_PACK(name value); \

#define USE_SHELL_METHODS(name,\
                          NAME,\
                          stub,\
                          type_n)\
    name NAME##_UNPACK(Basic basic){\
        struct Shell{\
            int type;\
            name* value;\
        };\
        if (basic->type == (type_n)) {\
            struct Shell* shell = (struct Shell*) calloc(1, sizeof(struct Shell));\
            shell->type = type_n;\
            shell->value = (name*) basic->value;\
            return *shell->value;\
        }\
        else\
            return stub;\
    }\
    Basic NAME##_PACK(name value){\
        struct Shell{\
            int type;\
            name* value;\
        };\
        struct Shell* shell = (struct Shell*) calloc(1, sizeof(struct Shell));\
        shell->type = type_n;\
        shell->value = (name*) calloc(1, sizeof(name));\
        *shell->value = value;\
        Basic basic = basic_new(shell);\
        return basic;\
    }

#define Basic struct _Basic*

struct _Basic{
    int type;
    void* value;

};

Basic basic_new(void* num);
void* get_value(Basic basic);

void bprint(Basic basic);
void bprintln(Basic basic);

#endif //COLLECTIONS_BASIC_H
