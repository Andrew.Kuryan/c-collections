//
// Created by andrew on 05.10.18.
//

#ifndef COLLECTIONS_STRING_H
#define COLLECTIONS_STRING_H

#include "basic.h"

#define String struct _String*
#define STR(s) string_to_chars(s)
#define STR_CLASS(s) basic_to_string(s)
#define STR_PACK(s) chars_to_basic(s)
#define STR_UNPACK(s) basic_to_chars(s)

struct _String{
    int type;
    char* chars;
};

String str_new(const char* str);
const char* string_to_chars(String str);
String basic_to_string(Basic basic);
Basic chars_to_basic(const char* str);
const char* basic_to_chars(Basic basic);

String str_scan_line();

int str_length(String s);
String str_concat(String s1, String s2);
char str_char_at(String s, int index);

int str_compare_to(String s1, String s2);
int str_compare_to_ignore_case(String s1, String s2);

int str_index_of(String s, char c);
int str_index_of_pos(String s, char c, int pos);
int str_last_index_of(String s, char c);
int str_last_index_of_pos(String s, char c, int pos);

String str_to_upper_case(String s);
String str_to_lower_case(String s);
String str_trim(String s);

String str_value_of(Basic basic);

#endif //COLLECTIONS_STRING_H
