//
// Created by andrew on 05.10.18.
//

#ifndef COLLECTIONS_INTEGER_H
#define COLLECTIONS_INTEGER_H

#include "basic.h"
#include "string.h"

#define Integer struct _Integer*
#define INT(a) int_class_to_int(a)
#define INT_CLASS(a) basic_to_int_class(a)
#define INT_PACK(a) int_to_basic(a)
#define INT_UNPACK(a) basic_to_int(a)

struct _Integer{
    int type;
    int* value;
};

Integer int_new(int a);
int int_class_to_int(Integer _integer);
Integer basic_to_int_class(Basic basic);
Basic int_to_basic(int a);
int basic_to_int(Basic basic);

Integer parse_int(String str);

#endif //COLLECTIONS_INTEGER_H
