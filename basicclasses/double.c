//
// Created by andrew on 05.10.18.
//

#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include "include/double.h"
#include "include/types.h"

Double double_new(double a){
    Double _double = (Double) calloc(1, sizeof(struct _Double));

    _double->type = DOUBLE_T;
    _double->value = (double*) calloc(1, sizeof(double));
    *_double->value = a;

    return _double;
}

Double parse_double(String str){
    String str1 = str_trim(str);
    double num = 0.0;
    int flag_sign = 0;

    int pos = 0;
    if (str_char_at(str1, pos) == '+') {
        flag_sign = 1;
        pos++;
    }
    else if (str_char_at(str1, pos) == '-') {
        flag_sign = -1;
        pos++;
    }
    while (str_char_at(str1, pos) != '\0' && str_char_at(str1, pos) != '.'){
        if (str_char_at(str1, pos) >= '0' && str_char_at(str1, pos) <= '9'){
            int d = str_char_at(str1, pos) - '0';
            if (num == 0)
                num = (double) d;
            else
                num = num * 10 + (double) d;
        }
        pos++;
    }
    int digit = 0;
    while (str_char_at(str1, pos) != '\0'){
        if (str_char_at(str1, pos) >= '0' && str_char_at(str1, pos) <= '9'){
            int d = str_char_at(str1, pos) - '0';
            digit++;
            num = num + (double) d / pow(10, digit);
        }
        pos++;
    }

    if (flag_sign == -1)
        num = -num;
    return double_new(num);
}

double double_class_to_double(Double _double){
    return *_double->value;
}

Double basic_to_double_class(Basic basic){
    if (basic->type == DOUBLE_T) {
        Double _double = (Double) calloc(1, sizeof(struct _Double));

        _double->type = DOUBLE_T;
        _double->value = (double*) basic->value;

        return _double;
    }
    else
        return NULL;
}

Basic double_to_basic(double a){
    Double aDouble = double_new(a);
    Basic basic = basic_new(aDouble);
    return basic;
}

double basic_to_double(Basic basic){
    Double aDouble = DOUBLE_CLASS(basic);
    if (aDouble != NULL)
        return *aDouble->value;
    else
        return 0.0;
}