//
// Created by andrew on 30.11.18.
//

#ifndef COLLECTIONS_BASICCLASSES_H
#define COLLECTIONS_BASICCLASSES_H

#include "include/basic.h"
#include "include/integer.h"
#include "include/double.h"
#include "include/string.h"

#endif //COLLECTIONS_BASICCLASSES_H
