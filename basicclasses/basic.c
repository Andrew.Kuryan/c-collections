//
// Created by andrew on 05.10.18.
//

#include <stdlib.h>
#include <stdio.h>
#include "include/basic.h"

Basic basic_new(void* num){
    Basic basic = (Basic) calloc(1, sizeof(struct _Basic));

    Basic view = (Basic) num;
    basic->type = view->type;
    //printf("%d : ", view->type);
    switch (view->type){
        case INT_T:
            //printf("%d\n", *((int*) view->value));
            basic->value = view->value;
            break;
        case DOUBLE_T:
            //printf("%lf\n", *((double *) view->value));
            basic->value = view->value;
            break;
        case STRING_T:
            //printf("%s\n", ((char *) view->value));
            basic->value = view->value;
            break;
        case CLASS_T:
            basic->value = view->value;
            //printf("class value\n");
            break;
        default:
            return NULL;
    }

    return basic;
}

void* get_value(Basic basic){
    return basic->value;
}

void bprint(Basic basic){
    switch (basic->type){
        case INT_T:
            printf("%d", *((int*) basic->value));
            break;
        case DOUBLE_T:
            printf("%lf", *((double*) basic->value));
            break;
        case STRING_T:
            printf("%s", ((char *) basic->value));
            break;
        default:
            printf("%s", MARE_STR(basic->type));
    }
}

void bprintln(Basic basic){
    switch (basic->type){
        case INT_T:
            printf("%d\n", *((int*) basic->value));
            break;
        case DOUBLE_T:
            printf("%lf\n", *((double*) basic->value));
            break;
        case STRING_T:
            printf("%s\n", ((char *) basic->value));
            break;
        default:
            printf("%s\n", MARE_STR(basic->type));
    }
}