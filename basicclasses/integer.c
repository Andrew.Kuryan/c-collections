//
// Created by andrew on 05.10.18.
//

#include <stdlib.h>
#include <stdio.h>
#include <zconf.h>
#include "include/integer.h"
#include "include/types.h"

Integer int_new(int a){
    Integer _integer = (Integer) calloc(1, sizeof(struct _Integer));

    _integer->type = INT_T;
    _integer->value = (int*) calloc(1, sizeof(int));
    *_integer->value = a;

    return _integer;
}

Integer parse_int(String str){
    String str1 = str_trim(str);
    int num = 0;
    int flag_sign = 0;
    for (int i=0; i<str_length(str1); i++){
        if (str_char_at(str1, i) >= '0' && str_char_at(str1, i) <= '9'){
            int d = str_char_at(str1, i) - '0';
            if (num == 0)
                num = d;
            else {
                if ((long) num * 10 + d < INT_MAX)
                    num = num * 10 + d;
                else {
                    if (flag_sign == -1)
                        num = -num;
                    return int_new(num);
                }
            }
        }
        else if (!flag_sign && str_char_at(str1, i) == '+')
            flag_sign = 1;
        else if (!flag_sign && str_char_at(str1, i) == '-')
            flag_sign = -1;
        else
            return NULL;
    }
    if (flag_sign == -1)
        num = -num;
    return int_new(num);
}

int int_class_to_int(Integer _integer){
    return *_integer->value;
}

Integer basic_to_int_class(Basic basic){
    if (basic->type == INT_T) {
        Integer _integer = (Integer) calloc(1, sizeof(struct _Integer));

        _integer->type = INT_T;
        _integer->value = (int*) basic->value;

        return _integer;
    }
    else
        return NULL;
}

Basic int_to_basic(int a){
    Integer integer = int_new(a);
    Basic basic = basic_new(integer);
    return basic;
}

int basic_to_int(Basic basic){
    Integer integer = INT_CLASS(basic);
    if (integer != NULL)
        return *integer->value;
    else
        return 0;
}