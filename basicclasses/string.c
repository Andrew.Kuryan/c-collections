//
// Created by andrew on 05.10.18.
//

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "include/string.h"
#include "include/types.h"
#include "include/integer.h"

String str_new(const char* str){
    String string = (String) calloc (1, sizeof(struct _String));

    string->type = STRING_T;
    string->chars = (char*) calloc(strlen(str), sizeof(char));
    strcpy(string->chars, str);

    return string;
}

String str_scan_line(){
    String string = (String) calloc(1, sizeof(struct _String));
    string->type = STRING_T;
    size_t len = 0;
    string->chars = (char*) calloc(0, sizeof(char));

    char c = ' ';
    while (c != '\n'){
        scanf("%c", &c);
        if (c != '\n'){
            len++;
            string->chars = (char*) realloc(string->chars, len);
            string->chars[len-1] = c;
        }
    }
    len++;
    string->chars = (char*) realloc(string->chars, len);
    string->chars[len-1] = '\0';

    return string;
}

String str_concat(String s1, String s2){
    String string = (String) calloc(1, sizeof(struct _String));
    string->type = STRING_T;

    string->chars = (char*) calloc(((size_t) str_length(s1)+ (size_t) str_length(s2)), sizeof(char));
    strcat(string->chars, string_to_chars(s1));
    strcat(string->chars, string_to_chars(s2));

    return string;
}

int str_length(String s){
    int length;
    char c = ' ';
    for (length=0; c != '\0'; length++){
        c = s->chars[length];
    }
    return length-1;
}

char str_char_at(String s, int index){
    return s->chars[index];
}

int str_compare_to(String s1, String s2){
    int result = 0;
    long int len1 = str_length(s1);
    long int len2 = str_length(s2);

    long int len = (len1 > len2) ? len2 : len1;
    for (int i=0; i<len; i++){
        if (str_char_at(s1, i) > str_char_at(s2, i)){
            result = 1;
            break;
        }
        else if (str_char_at(s2, i) > str_char_at(s1, i)){
            result = -1;
            break;
        }
    }
    if (!result){
        if (len1 < len2){
            result = -1;
        }
        else if (len2 < len1){
            result = 1;
        }
    }
    return result;
}

int str_compare_to_ignore_case(String s1, String s2){
    String s11 = str_to_upper_case(s1);
    String s21 = str_to_upper_case(s2);

    return str_compare_to(s11, s21);
}

int str_index_of(String s, char c){
    return str_index_of_pos(s, c, 0);
}

int str_index_of_pos(String s, char c, int pos){
    int ind = -1;
    for (int i=pos; i<str_length(s); i++){
        if (str_char_at(s, i) == c) {
            ind = i;
            break;
        }
    }
    return ind;
}

int str_last_index_of(String s, char c){
    return str_index_of_pos(s, c, str_length(s)-1);
}

int str_last_index_of_pos(String s, char c, int pos){
    int ind = -1;
    for (int i=pos; i>=0; i--){
        if (str_char_at(s, i) == c){
            ind = i;
            break;
        }
    }
    return ind;
}

String str_to_upper_case(String s){
    String string = (String) calloc(1, sizeof(struct _String));
    string->type = STRING_T;
    size_t len = 0;
    string->chars = (char*) calloc(len, sizeof(char));

    char c;
    for (int i=0; i<str_length(s); i++){
        c = str_char_at(s, i);
        if (c >= 'a' && c <= 'z'){
            c = (char) (c - ('a'-'A'));
        }
        len++;
        string->chars = (char*) realloc(string->chars, len);
        string->chars[i] = c;
    }
    return string;
}

String str_to_lower_case(String s){
    String string = (String) calloc(1, sizeof(struct _String));
    string->type = STRING_T;
    size_t len = 0;
    string->chars = (char*) calloc(len, sizeof(char));

    char c;
    for (int i=0; i<str_length(s); i++){
        c = str_char_at(s, i);
        if (c >= 'A' && c <= 'Z'){
            c = (char) (c + ('a'-'A'));
        }
        len++;
        string->chars = (char*) realloc(string->chars, len);
        string->chars[i] = c;
    }
    return string;
}

String str_trim(String s){
    String string = (String) calloc(1, sizeof(struct _String));
    string->type = STRING_T;
    size_t len = 0;
    string->chars = (char*) calloc(len, sizeof(char));

    int pos = 0;
    while (str_char_at(s, pos) == ' ')
        pos++;
    while (str_char_at(s, pos) != '\0' && str_char_at(s, pos) != ' '){
        len++;
        string->chars = (char*) realloc(string->chars, len);
        string->chars[len-1] = str_char_at(s, pos);
        pos++;
    }
    len++;
    string->chars = (char*) realloc(string->chars, len);
    string->chars[len-1] = '\0';

    return string;
}

//type to string
String str_value_of(Basic basic){
    String string = (String) calloc(1, sizeof(struct _String));
    string->type = STRING_T;
    string->chars = (char*) calloc(0, sizeof(char));

    int sign_flag = 0;
    int *i_num_ptr;
    int i_num;
    double *d_num_ptr;
    double d_num;
    int int_part, fract_part;
    int c;
    char t;

    long num_length = 0;
    switch (basic->type){
        case STRING_T:
            return STR_CLASS(basic);
        case INT_T:
            i_num_ptr = (int*) basic->value;
            i_num = *i_num_ptr;
            if (i_num == 0){
                string->chars = (char*) realloc(string->chars, 2);
                string->chars[0] = '0';
                string->chars[1] = '\0';
                return string;
            }
            if (i_num < 0) {
                sign_flag = 1;
                i_num = -i_num;
            }
            for (num_length = 0; i_num != 0; num_length++){
                c = i_num % 10;
                string->chars = (char*) realloc(string->chars, (size_t) (num_length+1));
                for (long i=num_length; i>0; i--){
                    t = string->chars[i];
                    string->chars[i] = string->chars[i-1];
                    string->chars[i-1] = t;
                }
                string->chars[0] = (char) (c+'0');
                i_num = i_num / 10;
            }
            if (sign_flag){
                string->chars = (char*) realloc(string->chars, (size_t) (num_length+1));
                for (long i=num_length; i>0; i--){
                    t = string->chars[i];
                    string->chars[i] = string->chars[i-1];
                    string->chars[i-1] = t;
                }
                string->chars[0] = '-';
                num_length++;
            }
            string->chars = (char*) realloc(string->chars, (size_t) (num_length+1));
            string->chars[num_length] = '\0';
            break;
        case DOUBLE_T:
            d_num_ptr = (double*) basic->value;
            d_num = *d_num_ptr;
            if (d_num == 0){
                string->chars = (char*) realloc(string->chars, 9);
                string->chars[0] = '0';
                string->chars[1] = '.';
                for (int i=2; i<8; i++)
                    string->chars[i] = '0';
                string->chars[8] = '\0';
                return string;
            }
            int_part = (int) d_num;
            fract_part = (int) ((d_num-int_part) * 10E5);
            if (fract_part < 0)
                fract_part = -fract_part;
            String str_int = str_value_of(basic_new(int_new(int_part)));
            String str_fract = str_value_of(basic_new(int_new(fract_part)));
            string = str_concat(string, str_int);
            string = str_concat(string, str_new("."));
            string = str_concat(string, str_fract);
            break;
        default:
            return str_new(MARE_STR(basic->type));
    }

    return string;
}

const char* string_to_chars(String str){
    return str->chars;
}

String basic_to_string(Basic basic){
    if (basic->type == STRING_T){
        String string = (String) calloc (1, sizeof(struct _String));

        string->type = STRING_T;
        string->chars = (char*) basic->value;

        return string;
    }
    else
        return NULL;
}

Basic chars_to_basic(const char* str){
    String string = str_new(str);
    Basic basic = basic_new(string);
    return basic;
}

const char* basic_to_chars(Basic basic){
    String string = STR_CLASS(basic);
    if (string != NULL)
        return string->chars;
    else
        return "";
}