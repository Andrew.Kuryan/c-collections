## Basicclasses

* ### Basic:
    * #### Usage:
    ```java
    //creating new basic odjects
    Integer i = int_new(10);
    Double d = double_new(5.5);
    String s = str_new("abc");

    //converting basic objects to Basic
    Basic b1 = basic_new(i);
    Basic b2 = basic_new(d);
    Basic b3 = basic_new(s);

    //usage of output methods
    bprintln(b1);                    //displays 10
    bprintln(b2);                    //displays 5.500000
    bprint(b3);                      //displays abc

    //unpacking from Basic to primitive types
    int i1 = INT_UNPACK(b1);         //i1 = 10
    double d1 = DOUBLE_UNPACK(b2);   //d1 = 5.5
    const char* s1 = STR_UNPACK(b3); //s1 = "abc"
    int i2 = INT_UNPACK(b2);         //Error! i2 = 0
    ```
* ### String:
    * #### Methods:
        method | return value | description 
        --- | --- | --- | 
        str_new(const char* str) | String | creates new string from const char array and returns it
        str_scan_line() | String | wating for user's intput and returns input value 
        str_length(String s) | int | returns the length of this string
        str_concat(String s1, String s2) | String | concatenates the string s1 to the end of s2 and returns resulting string
        str_char_at(String s, int index) | char | returns the char value at the specified index
        str_compare_to(String s1, String s2) | int | compares two strings lexicographically
        str_compare_to_ignore_case(String s1, String s2) | int | compares two strings lexicographically, ignoring case differences
        str_index_of(String s, char c) | int | returns the index within string s of the first occurrence of the specified character
        str_index_of_pos(String s, char c, int pos) | int | returns the index within string s of the first occurrence of the specified character, starting the search at the specified index
        str_last_index_of(String s, char c) | int | returns the index within string s of the last occurrence of the specified character
        str_last_index_of_pos(String s, char c, int pos) | int | returns the index within string s of the last occurrence of the specified character, searching backward starting at the specified index
        str_to_upper_case(String s) | String | converts all of the characters in string s to upper case and returns resulting string
        str_to_lower_case(String s) | String | converts all of the characters in string s to lower case and returns resulting string
        str_trim(String s) | String | returns a copy of the string, with leading and trailing whitespace omitted
        str_value_of(Basic basic) | String | returns the string representation of the Basic argument
    * #### Usage:
    ```java
    //creating new String
    String s = str_new("abc");
    printf("%s\n", STR(s));          //displays abc

    //concatenation of two Strings
    String s1 = str_new("ab");
    String s2 = str_new("cd");
    String s = str_concat(s1, s2);   //s = "abcd"

    //comparison of two Strings
    String s1 = str_new("abc");
    String s2 = str_new("bcd");
    int res = str_compare_to(s1, s2);//res = -1
    res = str_compare_to(s2, s1);    //res = 1

    //parsing Strings from basic types
    Integer i = int_new(10);
    Double d = double_new(5.5);
    String s = str_new("abc");
    String s1;
    s1 = str_value_of(BASIC(i));     //s1 = "10"
    s1 = str_value_of(BASIC(d));     //s1 = "5.500000"
    s1 = str_value_of(BASIC(s));     //s1 = "abc"
    ```
* ### Integer:
    * #### Usage:
    ```java
    //creating new Integer
    Integer i = int_new(15);
    printf("%d", INT(i));            //displays 15

    //parsing Integer from String
    String s = str_new("  25 ");
    Integer i = parse_int(s);        //i = 25
    s = str_new(" 25b");
    i = parse_int(s);                //i = NULL
    ```
* ### Double:
    * #### Usage:
    ```java
    //creating new Double
    Double d = double_new(5.5);
    printf("%lf", DOUBLE(d));        //displays 5.500000

    //parsing Double from String
    String s = str_new("  67.85 ");
    Double d = parse_double(s);      //d = 67.85
    ```

## Lists

* ### ArrayList:

    * #### Methods:
        method | return value | description 
        --- | --- | --- | 
        array_list_new(int type) | ArrayList | creates new ArrayList of argument type and returns it
        array_list_to_string(ArrayList arrayList) | String | returns the string representation of the ArrayList
        array_list_add(ArrayList arrayList, Basic basic) | int | adds new element in the arrayList with value basic and returns 1 in case of success
        array_list_get(ArrayList arrayList, int pos) | Basic | returns basic value of the array element on the position pos
        array_list_set(ArrayList arrayList, Basic newVal, int pos) | int | changes the value on the position pos by the value newVal and returns 1 in the case of success
        array_list_delete(ArrayList arrayList, int pos) | Basic | deletes element on the position pos and returns deleting value
        array_list_insert(ArrayList arrayList, int pos, Basic val) | int | inserts element with value val after the element on the position pos and returns 1 in the case of success
        array_list_size(ArrayList arrayList) | int | returns the size of arrayList

    * #### Usage:
    ```java 
    //creating new ArrayList
    ArrayList arrayList = array_list_new(INT_T);
    //adding three elements
    array_list_add(arrayList, INT_PACK(5));
    array_list_add(arrayList, INT_PACK(6));
    array_list_add(arrayList, INT_PACK(7));
    bprintln(BASIC(array_list_to_string(arrayList)));    //displays [5, 6, 7]

    //changing the value on the position 1
    array_list_set(arrayList, INT_PACK(8), 1);
    bprintln(BASIC(array_list_to_string(arrayList)));    //displays [5, 8, 7]

    //inserting the value on the position 3
    array_list_insert(arrayList, 2, INT_PACK(10));
    bprintln(BASIC(array_list_to_string(arrayList)));    //displays [5, 8, 10, 7]

    //deleting two elements
    array_list_delete(arrayList, 0);
    array_list_delete(arrayList, 2);
    bprintln(BASIC(array_list_to_string(arrayList)));    //displays [8, 10]

    //displaying arrayList's size
    printf("%d\n", array_list_size(arrayList));          //displays 2
    ```   
    * #### Tracing all elements of array:
    ```java
    ArrayList arrayList = array_list_new(INT_T);
    array_list_add(arrayList, INT_PACK(5));
    array_list_add(arrayList, INT_PACK(6));
    array_list_add(arrayList, INT_PACK(7));
    //displaying all elements of array
    FOREACH(t, arrayList){
        bprintln(t);                                     //displays 5, 6, 7
    }
    //incrementing all elements of array
    FOREACH(t, arrayList){
        int* i = (int*) VAL(t);
        (*i)++;
    }
    bprintln(BASIC(array_list_to_string(arrayList)));    //displays [6, 7, 8]
    ```

## Maps

* ### Map:

    * #### Methods:
        method | return value | description 
        --- | --- | --- | 
        map_new(int kType, int vType) | Map | creates new Map with kType of keys and vType of values and returns it
        map_to_string(Map map) | String | returns the string representation of the map
        map_put(Map map, Basic key, Basic value) | int | adds new record with with argument's key and value and returns 1 in the case of success
        map_get(Map map, Basic key) | Basic | returns the value of element with argument's key
        map_set(Map map, Basic key, Basic newVal) | int | changes the value of element with argument's key by the value newVal and returns 1 in the case of success
        map_delete(Map map, Basic key) | Basic | deletes element with the argument's key and returns deleting value
        map_key_set(Map map) | ArrayList | returns the ArrayList representation of the map's key set
        map_size(Map map) | int | returns the size of map

    * #### Usage
    ```java
    //creating new map
    Map map = map_new(STRING_T, DOUBLE_T);
    //adding three elements
    map_put(map, STR_PACK("pi"), DOUBLE_PACK(M_PI));
    map_put(map, STR_PACK("exp"), DOUBLE_PACK(M_E));
    map_put(map, STR_PACK("exp^2"), DOUBLE_PACK(pow(M_E, 2)));
    bprintln(BASIC(map_to_string(map)));                //displays [pi=3.141592, exp=2.718281, exp^2=7.389056]

    //getting element with key "exp"
    Basic b = map_get(map, STR_PACK("exp"));
    bprintln(b);                                        //displays 2.718282

    //tracing all values of the map
    ArrayList keys = map_key_set(map);
    FOREACH(k, keys){
        bprintln(map_get(map, k));                      //displays 3.141593, 2.718282, 7.389056
    }
    ```
