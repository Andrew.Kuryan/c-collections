//
// Created by andrew on 06.10.18.
//

#include "include/arraylist.h"
#include <stdio.h>

ArrayList array_list_new(int type){
    ArrayList arrayList = (ArrayList) calloc(1, sizeof(struct _ArrayList));

    arrayList->type = type;
    arrayList->size = 0;
    arrayList->head = NULL;
    arrayList->tail = NULL;

    return  arrayList;
}

String array_list_to_string(ArrayList arrayList){
    String res = str_new("");
    res = str_concat(res, str_new("["));
    for (int i=0; i<array_list_size(arrayList)-1; i++){
        res = str_concat(res, str_value_of(array_list_get(arrayList, i)));
        res = str_concat(res, str_new(", "));
    }
    res = str_concat(res, str_value_of(array_list_get(arrayList, array_list_size(arrayList)-1)));
    res = str_concat(res, str_new("]"));
    return res;
}

int array_list_size(ArrayList arrayList){
    return arrayList->size;
}

int array_list_add(ArrayList arrayList, Basic basic){
    if (basic->type != arrayList->type)
        return 0;
    if (arrayList->head == NULL){
        arrayList->head = list_element_new(basic);
        arrayList->tail = arrayList->head;
        arrayList->size++;
    }
    else{
        ListElement element = list_element_new(basic);
        arrayList->tail->next = element;
        arrayList->tail = element;
        arrayList->size++;
    }
    return 1;
}

Basic array_list_get(ArrayList arrayList, int pos){
    if (pos >= arrayList->size || pos < 0){
        return NULL;
    }
    ListElement temp = arrayList->head;
    int pos_t = 0;
    while (pos_t != pos){
        temp = temp->next;
        pos_t++;
    }
    return temp->value;
}

int array_list_set(ArrayList arrayList, Basic newVal, int pos){
    if (newVal->type != arrayList->type)
        return 0;
    *array_list_get(arrayList, pos) = *newVal;
    return 1;
}

Basic array_list_delete(ArrayList arrayList, int pos){
    if (pos >= arrayList->size || pos < 0)
        return 0;
    ListElement temp;
    if (pos == 0) {
        temp = arrayList->head;
        arrayList->head = arrayList->head->next;
        arrayList->size--;
        return temp->value;
    }
    else {
        temp = arrayList->head->next;
        ListElement temp_prev = arrayList->head;
        int pos_t = 1;
        while (pos_t != pos) {
            temp = temp->next;
            temp_prev = temp_prev->next;
            pos_t++;
        }
        temp_prev->next = temp->next;
        arrayList->size--;
        return temp->value;
    }
}

int array_list_insert(ArrayList arrayList, int pos, Basic val){
    if (val->type != arrayList->type)
        return 0;
    if (pos > arrayList->size || pos < 0)
        return 0;
    if (pos == 0){
        ListElement element = list_element_new(val);
        element->next = arrayList->head;
        arrayList->head = element;
        arrayList->size++;
    }
    else if (pos == arrayList->size){
        ListElement element = list_element_new(val);
        element->next = NULL;
        arrayList->tail->next = element;
        arrayList->tail = element;
        arrayList->size++;
    }
    else{
        ListElement temp = arrayList->head;
        int pos_t = 0;
        while (pos_t != pos-1){
            temp = temp->next;
            pos_t++;
        }
        ListElement element = list_element_new(val);
        element->next = temp->next;
        temp->next = element;
        arrayList->size++;
    }
    return 1;
}