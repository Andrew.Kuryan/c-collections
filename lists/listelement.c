//
// Created by andrew on 06.10.18.
//

#include <stdlib.h>
#include "include/listelement.h"

ListElement list_element_new(Basic basic){
    ListElement element = (ListElement) calloc(1, sizeof(struct _ListElement));

    element->value = basic;
    element->next = NULL;

    return element;
}

void list_element_destroy(ListElement element){
    free(element->value->value);
    free(element->value);
    free(element);
}