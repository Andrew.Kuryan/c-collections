//
// Created by andrew on 06.10.18.
//

#ifndef COLLECTIONS_ARRAYLIST_H
#define COLLECTIONS_ARRAYLIST_H

#include "listelement.h"
#include "../../basicclasses/include/string.h"

#define ArrayList struct _ArrayList*

ListElement foreach_temp_head_element;
#define FOREACH(t, al) \
foreach_temp_head_element = (al)->head;\
for (Basic (t) = foreach_temp_head_element->value; foreach_temp_head_element != NULL && ((t) = foreach_temp_head_element->value) != NULL;\
    foreach_temp_head_element = foreach_temp_head_element->next)\


struct _ArrayList{
    int type;
    int size;
    ListElement head;
    ListElement tail;
};

ArrayList array_list_new(int type);
String array_list_to_string(ArrayList arrayList);

int array_list_add(ArrayList arrayList, Basic basic);
Basic array_list_get(ArrayList arrayList, int pos);
int array_list_set(ArrayList arrayList, Basic newVal, int pos);
Basic array_list_delete(ArrayList arrayList, int pos);
int array_list_insert(ArrayList arrayList, int pos, Basic val);
int array_list_size(ArrayList arrayList);

#endif //COLLECTIONS_ARRAYLIST_H
