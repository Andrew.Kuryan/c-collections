//
// Created by andrew on 06.10.18.
//

#ifndef COLLECTIONS_LISTELEMENT_H
#define COLLECTIONS_LISTELEMENT_H

#include "../../basicclasses/include/basic.h"

#define ListElement struct _ListElement*

struct _ListElement{
    Basic value;
    ListElement next;
};

ListElement list_element_new(Basic basic);
void list_element_destroy(ListElement element);

#endif //COLLECTIONS_LISTELEMENT_H
